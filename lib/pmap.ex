defmodule Pmap do
  defmacro thunk(t) do
    quote do
      fn() -> unquote(t) end
    end
  end
  defmacro expand(do: x) do
    Macro.expand(x, __CALLER__) |> Macro.escape
  end
  def map(l, f) do
    Enum.map(l, &run(f, &1)) |> Enum.map(&acc/1)
  end
  def run(f, v) do
    p = self
    r = make_ref
    q = spawn_link(thunk(p <- {self, r, f.(v)}))
    {q, r}
  end
  defp acc({p, r}) do
    receive do
      {^p, ^r, x} ->
        x
    end
  end
end
