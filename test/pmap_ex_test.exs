defmodule PmapExTest do
  use ExUnit.Case
  import PmapExTest.Helpers
  def l(n//100000), do: 1..n

  test "map id" do
    check(l, &(&1))
  end
  test "map sq" do
    check(l, &(&1 * &1))
  end
end
