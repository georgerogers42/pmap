defmodule PmapExTest.Helpers do
  defmacro check(l, f) do
    quote do
      l = unquote(l)
      f = unquote(f)
      assert(Pmap.map(l, f) === Enum.map(l, f))
    end
  end
end
ExUnit.start
